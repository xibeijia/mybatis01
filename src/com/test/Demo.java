package com.test;

import com.fz.entity.Student;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.util.List;

/**
 * Created by webrx on 2017-08-26.
 */
public class Demo {
    public static void main(String[] args) throws IOException {
        //建立数据库会话工厂
        SqlSessionFactory sf = new SqlSessionFactoryBuilder().build(Resources.getResourceAsReader("mybatis-config.xml"));

        //读取会话
        SqlSession s = sf.openSession();
        System.out.println(s);

        //实现删除操作
        //s.delete("deleteById",1);

        //实现查询操作 返回List结果
        List<Student> sts = s.selectList("query");
        System.out.println(sts.size());
        for (Student st : sts) {
            System.out.println(st.getName());
        }
        s.commit();
    }
}
