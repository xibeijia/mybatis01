mybatis
MyBatis 本是apache的一个开源项目iBatis, 
2010年这个项目由apache software foundation 迁移到了google code，
并且改名为MyBatis 。
2013年11月迁移到Github。
MyBatis 是一款优秀的持久层框架，它支持定制化 SQL、存储过程以及高级映射。
MyBatis 避免了几乎所有的 JDBC 代码和手动设置参数以及获取结果集。
MyBatis 可以使用简单的 XML 或注解来配置和映射原生信息，
将接口和 Java 的 POJOs(Plain Old Java Objects,普通的 Java对象)映射成数据库中的记录。

	对jdbc操作的框架（orm 持久层 )
	dbutils
	hibernate
	mybatis

官方网站
	http://www.mybatis.org

官方中文使用手册
	http://www.mybatis.org/mybatis-3/

mybatis 框架包所有网站  github.com

	https://github.com/mybatis/mybatis-3/releases
	下载 mybatis-3.4.5.zip


mybatis 3.4.5入门实例
-----------------------------
1)建立java项目，添加jar文件
	mybatis-3.4.5.jar
	mysql-5.1.40-bin.jar

2)在src目录下建立mybatis-config.xml文件，为项目框架核心配置文件
	<?xml version="1.0" encoding="UTF-8" ?>
	<!DOCTYPE configuration
		PUBLIC "-//mybatis.org//DTD Config 3.0//EN"
		"http://mybatis.org/dtd/mybatis-3-config.dtd">
	<configuration>
	    <typeAliases>
		<!-- 实体类别名 -->
		<typeAlias alias="student" type="com.fz.entity.Student"/>
		<!-- 将实体包下的类自动为别名 com.fz.entity.User  别名就是 user-->
		<package name="com.fz.entity"/>
	    </typeAliases>
	    <environments default="development">
		<environment id="development">
		    <transactionManager type="JDBC"/>
		    <dataSource type="POOLED">
			<property name="driver" value="com.mysql.jdbc.Driver"/>
			<property name="url" value="jdbc:mysql://localhost:3306/db?useUnicode=true&amp;characterEncoding=utf8&amp;useSSL=true"/>
			<property name="username" value="root"/>
			<property name="password" value="root"/>
		    </dataSource>
		</environment>
	    </environments>
	    <mappers>
		<mapper resource="com/fz/mapper/StudentMapper.xml"/>
	    </mappers>
	</configuration>

3)准备数据库
	localhost 3306  db数据库  账号root 密码root

	CREATE DATABASE db;
	USE db;
	CREATE TABLE db_student(
	    id INT UNSIGNED AUTO_INCREMENT,
	    NAME VARCHAR(20),
	    score TINYINT UNSIGNED,
	    address VARCHAR(100), 
	    
	    PRIMARY KEY(id)
	);
	INSERT INTO db_student VALUES(NULL,'李四',80,'郑州'),(NULL,'张三',60,'北京');

	SELECT * FROM db_student;	
4)建立模型类 实体类
	package com.fz.entity;
	import lombok.AllArgsConstructor;
	import lombok.Data;
	import lombok.NoArgsConstructor;
	/**
	 * Created by webrx on 2017-08-22.
	 */
	@Data @NoArgsConstructor @AllArgsConstructor
	public class Student {
	    private int id;
	    private String name;
	    private int score;
	    private String address;
	}

	如果注解方式不能用 在idea中打开 Settings
	Build,Execution,Deployment
	>Compliler
		Annotation Processors  
			Enable annotation processing 打上对勾，启用


5)建立实例映射文件在实体类同一路径下 com/fz/mapper/StudentMapper.xml文件
	<?xml version="1.0" encoding="UTF-8" ?>
	<!DOCTYPE mapper
		PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN"
		"http://mybatis.org/dtd/mybatis-3-mapper.dtd">

	<!-- com/fz/mampepr/StudentMapper.xml 影射配置文件位置 -->
	<mapper namespace="com.fz.mapper.StudentMapper">

	    <!-- 此处结果类型 resultType 指定的student 为com.fz.entity.Student实体类别名 -->
	    <select id="query" resultType="student">
		select * from db_student
	    </select>

	    <delete id="deleteById" parameterType="int">
		delete from db_student where id=#{id}
	    </delete>
	</mapper>

6)编写demo程序直接测试
	//建立数据库会话工厂
        SqlSessionFactory sf = new SqlSessionFactoryBuilder().build(Resources.getResourceAsReader("mybatis-config.xml"));

        //读取会话
        SqlSession s = sf.openSession();
        System.out.println(s);

        //实现删除操作
        s.delete("deleteById",2);

        //实现查询操作 返回List结果
        List<Student> sts = s.selectList("query");
        System.out.println(sts.size());
        for (Student st : sts) {
            System.out.println(st.getName());
        }
        s.commit();